# Rubén Vega Balbás' Personal Website, Portfolio, and Artist Shop

This project contains [www.ruvebal.art](http://www.ruvebal.art) frontend, based on the template:

```sh
npx create-remix@latest --template vercel/vercel/examples/remix
```

This [gitLab repository](https://gitlab.com/ruvebal/website) together with all its source code is originated and maintained by  **Ph.D. Rubén Vega Balbás** [ORCID \#0000-0001-6862-9081](https://orcid.org/0000-0001-6862-9081) with the exception of the declared third parties modules under their respective open source licenses.

### Contact @ruvebal
<`ruben.vega.baslbas@gmail.com`><br>

## Table of Contents

- [Running](#running)
    - [Dependencies](#dependencies)
    - [Environments](#environments)
    - [Make Rules](#make-rules)
- [Testing](#testing)
    - [Dependencies](#dependencies-2)
    - [Make Rule](#make-rule-2)

## Deployment

[![Deployed with Vercel CLI](https://vercel.com/docs/cli)]

```sh
npm i -g vercel
vercel
```

_Live Example: https://website-ruvebal.vercel.app/_


## Development

To run the Remix app locally, make sure the project's local dependencies are installed:

```sh
npm install
```

Afterwards, start the Remix development server like so:

```sh
npm run dev
```

Open up [http://localhost:5173](http://localhost:5173) and you should be ready to go!
