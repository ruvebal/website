// In a file like `utils/graphqlClient.js`
import { GraphQLClient } from 'graphql-request';

const arnetConfig = require('../../configs/hygraph.config');

export const client = new GraphQLClient('myGraphqlEndpoint', {
    headers: {
        authorization: 'Bearer YOUR_AUTH_TOKEN',
    },
});

// In your loader function in a route file like `routes/products.js`
/*
import { client } from '~/utils/graphqlClient';

export const loader = async () => {
    const query = `
    query {
      products {
        name
        description
        image
        availability
        slug
      }
    }
  `;
    const data = await client.request(query);
    return data;
};
*/