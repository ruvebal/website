const myGraphqlEndpoint = {
    contentAPI: 'https://api-eu-central-1-shared-euc1-02.hygraph.com/v2/clvwmd7vn000001w6f0k790hr/master',
    hiPerformanceAPI: 'https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clvwmd7vn000001w6f0k790hr/master',
    assetUploadAPI: 'https://api-eu-central-1-shared-euc1-02.hygraph.com/v2/clvwmd7vn000001w6f0k790hr/master/upload',
    managementAPI: 'https://management-eu-central-1-shared-euc1-02.hygraph.com/graphql'
};

module.exports = myGraphqlEndpoint;